%title: OpenCV
%author: xavki


 ██████╗ ██████╗ ███████╗███╗   ██╗ ██████╗██╗   ██╗
██╔═══██╗██╔══██╗██╔════╝████╗  ██║██╔════╝██║   ██║
██║   ██║██████╔╝█████╗  ██╔██╗ ██║██║     ██║   ██║
██║   ██║██╔═══╝ ██╔══╝  ██║╚██╗██║██║     ╚██╗ ██╔╝
╚██████╔╝██║     ███████╗██║ ╚████║╚██████╗ ╚████╔╝ 
 ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═══╝ ╚═════╝  ╚═══╝  


-----------------------------------------------------------------------                                                                             
# IA vs ML vs DL ??!!

<br>

IA : Intelligence Artificielle

ML : Machine learning (apprentissage automatique)

DL : Deep Learning (apprentissage profond)

<br>

Dans l'ordre

	IA > ML > DL

-----------------------------------------------------------------------                                                                             
# IA vs ML vs DL ??!!

<br>

IA : Intelligence Artificielle

	* date des années 50 (1956 - Université de Dartmouth) 
		* John McCarthy - Marvin Lee Minsky (cryo)
		* SNARC (réseau de 40 neurones d'un rat)

	* 1960 : premier projet de vision par ordinateur

	* 2001 : première détection (Viola & Jones)

	* automatisation de tâches réalisées par l'humain

	* sous entend la prise de décision

	* sous entend l'apprentissage/adaptation

	* du coup : gros volumes de données & capacité du hardware

	* changement progressif de l'interface avec les machines

	* évolution des tâches déléguées aux machines et de leur autonomie

	* débuts IA symbolique : règles de décisions basées sur les symboles

-----------------------------------------------------------------------                                                                             
# IA vs ML vs DL ??!!

<br>

ML : Machine Learning

	* plus récent années 80 avec les sytèmes experts
			* une base de faits
    	* une base de règles
    	* un moteur d'inférence

  * principe inversé par rapport à la programmation classique
      * avant : input + règles = output
      * après : input + output = règles

  * notion d'apprentissage avec un feedback
      * supervisé : fourni données + réponses
      * non supervisé : fourni que les données
      * renforcement : essais donnant lieu à erreurs (positif/négatif)

  * gros volumes de données (structurées ou non)

-----------------------------------------------------------------------                                                                             
# IA vs ML vs DL ??!!


<br>

DL : Deep Learning

	* plusieurs niveaux en réseau de neurones

-----------------------------------------------------------------------                                                                             
# IA vs ML vs DL ??!!


<br>

Computer Vision : 
	
	1- acquisition d'images (stockées, vidéos...)

	2- processer l'image (tranformer pour le traitement)

	3- interprétation de l'image par l'ordinateur
